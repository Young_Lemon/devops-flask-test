FROM python:3.9-alpine

WORKDIR /usr/src/app

RUN pip install --no-cache-dir Flask prometheus-flask-exporter

COPY . .

EXPOSE 5000

ENTRYPOINT [ "python" ]
CMD [ "app.py"]
